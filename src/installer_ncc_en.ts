<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>InstallerNCC</name>
    <message>
        <location filename="installerncc.cpp" line="96"/>
        <source>Installer for all fomod archives. Requires NCC to be installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="276"/>
        <source>failed to start %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="280"/>
        <source>Running external installer.
Based on Nexus Mod Manager by Black Tree Gaming Ltd.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="282"/>
        <source>Force Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="308"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="331"/>
        <source>installation failed (errorcode %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="342"/>
        <source>Confirm Mod Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="342"/>
        <source>Desired mod name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="453"/>
        <source>NCC is not installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="455"/>
        <source>NCC Version may be incompatible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="457"/>
        <source>.NET 4.6 or greater is required.</source>
        <oldsource>dotNet is not installed or outdated.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="459"/>
        <location filename="installerncc.cpp" line="477"/>
        <location filename="installerncc.cpp" line="488"/>
        <source>invalid problem key %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="467"/>
        <source>NCC is not installed. You won&apos;t be able to install some scripted mod-installers. Get NCC from &lt;a href=&quot;http://www.nexusmods.com/skyrim/mods/1334&quot;&gt;the MO page on nexus&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="470"/>
        <source>NCC version may be incompatible, expected version 0.%1.x.x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="installerncc.cpp" line="473"/>
        <source>&lt;li&gt;.NET version 4.6 or greater is required to use NCC. Get it from here: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/li&gt;</source>
        <oldsource>&lt;li&gt;dotNet is not installed or the wrong version. This is required to use NCC. Get it from here: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/li&gt;</oldsource>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
